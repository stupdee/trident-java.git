package org.tron.trident.service;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tron.trident.core.ApiWrapper;

import java.util.List;

@Service
public class TRXService {
    @Autowired
    private AccountService accountService;

    private Logger logger = LoggerFactory.getLogger(TRXService.class);

    public String createNewWallet(String account){
        logger.info("====>  Generate new wallet file for TRX.");
        List<String>  addressList = ApiWrapper.generateAddress();
        String address = addressList.get(0);
        String privateKey = addressList.get(1);
        System.out.println("地址"+account+"::"+addressList);
        accountService.saveOne(account, privateKey, address);
        return address;
    }
}
