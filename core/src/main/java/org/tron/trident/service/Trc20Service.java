package org.tron.trident.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.tron.trident.common.MessageResult;
import org.tron.trident.core.ApiWrapper;
import org.tron.trident.core.contract.Trc20Contract;
import org.tron.trident.core.exceptions.IllegalException;
import org.tron.trident.entity.Account;
import org.tron.trident.proto.Chain;
import org.tron.trident.proto.Response;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;


@Service
public class Trc20Service {
    private static Logger logger = LoggerFactory.getLogger(Trc20Service.class);

    @Value("${tron.contract-address}")
    private String contractAddress;//hex格式

    @Value("${tron.address}")
    private String address;//发币地址 hex格式

    @Value("${tron.private-key}")
    private String privateKey;//私钥

    //token的精度  就是小数点后面有多少位小数 然后1后面加多少个0就可以
    private static final BigDecimal decimal = new BigDecimal("1000000");


    public BigInteger balanceOf(String address) {
        ApiWrapper client = ApiWrapper.ofNile(privateKey);

        org.tron.trident.core.contract.Contract contract = client.getContract(contractAddress);

        Trc20Contract token = new Trc20Contract(contract, address, client);
        BigInteger balance = token.balanceOf(address);
        return balance;

    }

    public MessageResult feeAmtTransfer(Account account, long feeAmt) {
        //发起矿工费转账
        String txId = this.transferFee(account.getAddress(), feeAmt);
        logger.info(new Date() + "create new amount={},feeAmt={},txId={}", account, feeAmt, txId);
        return new MessageResult(0, "提交成功");

    }
    public String transferFee(String toAddress,long feeAmt)  {
        ApiWrapper client = ApiWrapper.ofNile(privateKey);
        try {
            logger.info("矿工费地址"+toAddress+"费用:"+feeAmt);
            Response.TransactionExtention result = client.transfer(address, toAddress, feeAmt*1000000);
            Chain.Transaction signedTransaction = client.signTransaction(result);
            String txId = client.broadcastTransaction(signedTransaction);
            logger.info("转账矿工费成功"+toAddress+":::"+txId);
            return  txId;
        }catch (Exception e){
            logger.error("矿工费转账失败："+e);
        }
        return null;
    }
    //用户合约中代币向归集中转账
    public String transferContract(String fromAddress,String privateKey,long amount)  {
        ApiWrapper client = ApiWrapper.ofNile(privateKey);
        try {

            org.tron.trident.core.contract.Contract contract = client.getContract(contractAddress);

            Trc20Contract token = new Trc20Contract(contract, fromAddress, client);

            String txId = token.transfer(address, amount, "代币转账", 1000000000L);

            return  txId;
        }catch (Exception e){
            logger.error("地址"+fromAddress +new Date()+"转账失败："+e);
        }
        return null;
    }
    //用户提现
    public MessageResult withdraw(String toAddress,long amount)  {
        ApiWrapper client = ApiWrapper.ofNile(privateKey);
        try {

            org.tron.trident.core.contract.Contract contract = client.getContract(contractAddress);

            Trc20Contract token = new Trc20Contract(contract, address, client);

            String txId = token.transfer(toAddress, amount, "代币转账", 1000000000L);
            MessageResult messageResult = new MessageResult();
            messageResult.setData(txId);
            return   messageResult;
        }catch (Exception e){
            logger.error("地址"+toAddress +new Date()+"转账失败："+e);
        }
        return null;
    }
}
